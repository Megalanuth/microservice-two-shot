from django.urls import reverse
from django.db import models


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)


    # def __str__(self):
    #     return str(self. import_href)


class Hat(models.Model):

    fabric = models.CharField(max_length=200)
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )


    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})

       # def __str__(self):
    #     return self.name




