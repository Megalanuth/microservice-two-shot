# Wardrobify

Team:

* Colin Summers - Shoes Microservice
* Shujin Pang - Hats Microservice

## Design

## Shoes microservice
Backend:

We will have 2 model classes: "BinVO" and "Shoe" for Shoes microservice:
1. Shoes Model class will include the following properties: manufacturer, model name, color, a picture URL.
2. the BinVo model class will include the folowing properties: closet_name, bin_number, bin_size and import_href.

Relationship of the two models:
The BinVO is served a a foreingn key to Shoe because one bin can store many shoes.

Poller:
The Shoes microservice will be constantly polling bin data from the wardrobe microservice and use that to update/create BinVo

API Endpoints:
api_list_shoes: This endpoint handles GET and POST requests to list all shoes or create a new shoe.
api_show_shoe: This endpoint handles DELETE requests to delete a shoe.



Frontend:
The frontend components will interact with these API endpoints to display a list of shoes, create a new shoe, and delete a shoe




## Hats microservice
Backend:

We will have 2 model classes: "LocationVO" and "Hat" for Hats microservice:
1. Hats Model class will include the following properties: fabric, its style name, its color, a picture URL,
2. the LocationVo model class will include the folowing properties: closet_name, section_number, shelf_number and import_href.

Relationship of the two models:
The LocationVO is served a a foreingn key to Hat because one location can store many hats

Poller:
The hats microservice will be constantly polling location data from the wardrobe microservice and use that to update/create LocationVo


API Endpoints:
api_list_hats: This endpoint handles GET and POST requests to list all hats or create a new hat.
api_show_hat: This endpoint handles GET, PUT, and DELETE requests to show details, update, or delete a hat.



Frontend:
The frontend components will interact with these API endpoints to display a list of hats, create a new hat, and delete a hat
