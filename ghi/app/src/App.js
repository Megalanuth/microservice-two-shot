import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatForm from './HatForm';
import ShoesList from './shoeslist';
import AddShoeForm from './AddShoeForm';

function App() {

  const [ hats, setHats ] = useState([]);
  const [ locations, setLocations ] = useState([]);
  const [ shoes, setShoes ] = useState([]);
  const [ bins, setBins ] = useState([]);

  async function getHats() {
    const response = await fetch('http://localhost:8090/api/hats/');
    if (response.ok) {
      const { hats } = await response.json();
      setHats(hats);
    } else {
      console.error('An error occurred fetching the data')
    }
  }

  async function getLocations() {
    const url = 'http://localhost:8100/api/locations/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  async function getShoes() {
    const response = await fetch('http://localhost:8080/api/shoes/')
    if (response.ok) {
      const { shoes } = await response.json();
      setShoes(shoes);
      // console.log(shoes)
    } else {
      console.error('An error occurred fetching the shoe data')
    }
  }

  async function getBins() {
    const response =await fetch('http://localhost:8100/api/bins/')
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins)
      // console.log(data.bins)
    } else {
      console.error('An error occurred fetching the bin data')
    }
  }

  useEffect(() => {
    getHats();
    getLocations();
    getShoes();
    getBins();
  }, [])

  if (hats === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatsList hats={hats} getHats={getHats}/>} />
            <Route path="new" element={<HatForm locations={locations} getHats={getHats} />} />
          </Route>
          <Route path="shoes">
            <Route index element={<ShoesList shoes={shoes} getShoes={getShoes}/>} />
            <Route path="new" element={<AddShoeForm bins={bins} getShoes={getShoes} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
