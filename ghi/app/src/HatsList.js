
function HatsList(props) {
    const handleDelete = async (hat) => {
      try {
        const deleteUrl = `http://localhost:8090/api/hats/${hat.id}/`;
        const fetchOptions = {
          method: 'DELETE',
        };
        const response = await fetch(deleteUrl, fetchOptions);
        if (response.ok) {
          props.getHats(); // Use props.getHats to call the function from the parent component
        } else {
          console.error('Error deleting the hat');
        }
      } catch (error) {
        console.error('An error occurred while deleting the hat:', error);
      }
    };

    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Style</th>
            <th>Fabric</th>
            <th>Color</th>
            <th>Location</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{hat.style}</td>
                <td>{hat.fabric}</td>
                <td>{hat.color}</td>
                <td>{hat.location}</td>
                <td>
                  <button onClick={() => handleDelete(hat)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default HatsList;









