import React, { useState, useEffect } from 'react';

function HatForm({ getHats, locations }) {
  const [location, setLocation] = useState('');
  const [style, setStyle] = useState('');
  const [fabric, setFabric] = useState('')
  const [color, setColor] = useState('')
  const [hasCreated, setHasCreated] = useState(false);



  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      style, fabric, color,location
    };

    const hatUrl = 'http://localhost:8090/api/hats/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const hatResponse = await fetch(hatUrl, fetchOptions);
    if (hatResponse.ok) {
      setLocation('');
      setStyle('')
      setFabric('');
      setHasCreated(true);
      getHats()
    }
  }

  function handleChangeLocation(event) {
    const { value } = event.target;
    setLocation(value);
  }

  function handleChangeStyle(event) {
    const { value } = event.target;
    setStyle(value);
  }

  function handleChangeFabric(event) {
    const { value } = event.target;
    setFabric(value);
  }
  function handleChangeColor(event) {
    const { value } = event.target;
    setColor(value);
  }

  let spinnerClasses = 'd-flex justify-content-center mb-3';
  let dropdownClasses = 'form-select d-none';
  if (locations.length > 0) {
    spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
    dropdownClasses = 'form-select';
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (hasCreated) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col col-sm-auto">
        <img width="400" className="bg-white rounded shadow d-block mx-auto mb-4" src="/image.jpeg" />
        </div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-hat-form">
                <h1 className="card-title">Lets Create A New Hat!</h1>
                <p className="mb-3">
                  Please choose which location
                  you'd like to add the hat to.
                </p>
                <div className={spinnerClasses} id="loading-location-spinner">
                  <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div className="mb-3">
                  <select value={location} onChange={handleChangeLocation} name="location" id="location" className={dropdownClasses} required>
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                      return (
                        <option key={location.href} value={location.href}>{location.closet_name}</option>
                      )
                    })}
                  </select>
                </div>
                <p className="mb-3">
                  Now, tell us about yourself.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input value={style} onChange={handleChangeStyle} required placeholder="Style Name" type="text" id="style" name="style" className="form-control" />
                      <label htmlFor="style">Style Name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input value={fabric} onChange={handleChangeFabric} required placeholder="Fibric" type="text" id="fibric" name="fibric" className="form-control" />
                      <label htmlFor="fibric">Fabric</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input value={color} onChange={handleChangeColor} required placeholder="Color" type="text" id="color" name="color" className="form-control" />
                      <label htmlFor="color">Color</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Ready to create!</button>
              </form>
              <div className={messageClasses} id="success-message">
                Congratulations! You have created a new hat!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}


export default HatForm;
