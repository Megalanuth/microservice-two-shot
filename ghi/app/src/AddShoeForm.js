import React, { useEffect, useState } from 'react';

function AddShoeForm({getShoes}) {
    const [ bins, setBins ] = useState([]);
    const [hasAddedShoe, setHasAddedShoe] = useState(false);
    //Condense all form data into one state object
    const [formData, setFormData] = useState({
        manufacturer: '',
        model_name: '',
        color: '',
        pictureURL:'',
        bin: '',
    })

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/bins/');
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }
    
    useEffect(() => {fetchData();}, []);

    const handleSubmit = async (event) => {
        console.log(formData);
        event.preventDefault();
        const url = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            //By using one formData state object,
            //it can pass directly into the request!
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
    
        if (response.ok) {
            //The single formData object allows for easier clearing of data
            setFormData({
                manufacturer: '',
                model_name: '',
                color: '',
                pictureURL:'',
                bin: '',
            });
            setHasAddedShoe(true);
            getShoes();
        }
    }
    
    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            //Spread/Copy previous form data into the new state object
            ...formData,
            //Then add the currently engaged input key and value
            [inputName]: value
        });
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasAddedShoe) {
      messageClasses = 'alert alert-success mb-0';
      formClasses = 'd-none';
    }

    return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a Shoe</h1>
          <form className={formClasses} onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Manufacturer" required type="text" 
                name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Model Name" required type="text" 
                name="model_name" id="model_name" className="form-control" />
              <label htmlFor="model_name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Color" required type="text" 
                name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange} placeholder="Picture URL" required type="text" 
                name="pictureURL" id="pictureURL" className="form-control" />
              <label htmlFor="pictureURL">Picture URL</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.href} value={bin.href}>
                      {bin.closet_name}, {bin.bin_number}
                    </option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Add This Shoe!</button>
          </form>
          <div className={messageClasses} id="success-message">
            Shoe is Added!
          </div>
        </div>
      </div>
    </div>
  );
}

export default AddShoeForm;
