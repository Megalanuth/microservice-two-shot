function ShoesList(props) {
    
    async function deleteShoe(shoeID){
        const url = `http://localhost:8080/api/shoes/${shoeID}/`;
        const fetchConfig = {method: "DELETE",};
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            props.getShoes();
        }
    }
    if (props.shoes === undefined){return null}
    return(
        <>
            <table className="table table-striped">
                <thead className="table-primary">
                    <tr>
                        <th>Color</th>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Bin</th>
                        <th>Delete?</th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes.map(shoe => {
                        return(
                            <tr key={shoe.id}>
                                <td>{ shoe.color }</td>
                                <td>{ shoe.manufacturer }</td>
                                <td>{ shoe.model_name }</td>
                                <td>{ shoe.bin }</td>
                                <td><button onClick={() => deleteShoe(shoe.id)}>Delete</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default ShoesList;
